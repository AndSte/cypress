/// <reference types="cypress" />
import Chance from 'chance';


describe('Test Add Sneaker', () => {

    const chance = new Chance();

    before(() => {
        cy.visit('/authorization/login');
    });

    it('should add Sneaker', () => {
        const nameSneakerLength = chance.prime({min: 3, max: 12});
        const nameSneaker = chance.string({ length: nameSneakerLength });
        const modelSneakerLength = chance.prime({min: 3, max: 12});
        const modelSneaker = chance.string({ length: modelSneakerLength });
        const categorySneakerLength = chance.prime({min: 3, max: 12});
        const categorySneaker = chance.string({ length: categorySneakerLength });
        const descriptionSneakerLength = chance.prime({min: 15, max: 100});
        const descriptionSneaker = chance.string({ length: descriptionSneakerLength });
        const priceSneaker = chance.integer({min: 100, max: 1000});

        cy.get('input[name="login"]').type('andrey@mail.com');
        cy.get('input[name="password"]').type('12345678');
        cy.get('button').click().url().should('include', '/');
        cy.get(".menu-icon").click();
        cy.get('.main-menu-block').find(`[href="/profile"]`);
        cy.get('.main-menu-block [href="/add"]').click().url().should('include', '/add');
        cy.get('input[placeholder="Name"]').type(nameSneaker);
        cy.get('input[placeholder="Model"]').type(modelSneaker);
        cy.get('input[placeholder="Category"]').type(categorySneaker);
        cy.get('[placeholder="Description"]').type(descriptionSneaker);
        cy.get('input[placeholder="Sizes"]').type('41, 42, 43, 44, 45');
        cy.get('input[type="number"]').type(priceSneaker);

        const amountImages = chance.prime({min: 1, max: 8});

        for(let i = 0; i < amountImages; i++) {
            const randomImage = chance.prime({ min: 0, max: 9 });
            cy.fixture(`images/${randomImage}.jpg`).as(`sneakerImage${i}`);
        }
        
        cy.get('input[type="file"]').then(function(input) {

            const list = new DataTransfer();

            for(let i = 0; i < amountImages; i++) {
                const blob = Cypress.Blob.base64StringToBlob(this[`sneakerImage${i}`], 'image/jpg');
                const file = new File([blob], `images/${i}.jpg`, { type: 'image/jpg' });
                list.items.add(file);
            }
            const myFileList = list.files;
            input[0].files = myFileList;
            
            input[0].dispatchEvent(new Event('change', { bubbles: true }))
        });
        cy.get('.button > button').click().intercept({ status: 200 });
    });
});