/// <reference types="cypress" />

import '../support/search.spec';

describe('Sneakers page', () => {
    before(() => {
        cy.visit('/').wait(500);
    });
    it('should visit sneakers page', () => {
        cy.get('[href="/sneakerPage/5fd500933d78a71e648e7db9"]').click().url().should('include', '/sneakerPage/');
        
    });
    

    it('should check inforamtion', () => {
        cy.get(".settingSneaker > h1").should('contain.text', "Adidas NMD_R1 SHOES");
        cy.get(".settingSneaker > h2").should('contain.text', "140");
        cy.get(".VueCarousel-inner").find('img');
        cy.get('.description > p').should('contain.text', "High-tech");
    });

    it('should test search', () => {
        cy.search();
    });
    
});