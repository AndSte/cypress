/// <reference types="cypress" />

import Chance from 'chance';

const chance = new Chance();

describe("Authorization", () => {
    const lengthLogin = chance.prime({min: 8, max: 16});
    const login = chance.string({ length: lengthLogin });
    const lengthPassword = chance.prime({min: 8, max: 16});
    const password = chance.string({ length: lengthPassword });

    beforeEach(() => {
        cy.visit('/authorization/login');    
    });
    it('shoud login', () => {
        cy.get('input[name="login"]').type('andrey2@mail.com');
        cy.get('input[name="password"]').type('12345678');
        cy.get('button').click().url().should('include', '/');
        cy.get(".menu-icon").click();
        cy.get('.main-menu-block').find(`[href="/profile"]`);
    });

    it('should wrong data', () => {
        cy.get('input[name="login"]').type(login);
        cy.get('input[name="password"]').type(password);
        cy.get('button').click().url().should('include', 'authorization');
        cy.get('.popup .item').should('contain.text', 'Неправильный логин или пароль!');
    });
    
});