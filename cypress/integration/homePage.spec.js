/// <reference types="cypress" />

import "../support/categoriesTest.spec";

describe("Home Page", () => {
  before(() => {
    cy.visit("/");
  });
  it("shoud be visible", () => {
    cy.get(".tovars").should("be.visible");
  });

  it("should be categories work", () => {
    cy.categories();
  })
  
});
