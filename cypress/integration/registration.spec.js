/// <reference types="cypress" />

import Chance from 'chance';

describe("Registration", () => {
    const chance = new Chance();
    const lengthLogin = chance.prime({min: 8, max: 16});
    const login = chance.string({ length: lengthLogin });
    const lengthPassword = chance.prime({min: 8, max: 16});
    const password = chance.string({ length: lengthPassword });
    const email = chance.email({ domain: 'mail.com' })

    beforeEach(() => {
        cy.visit('/authorization/registration');
    });

    it('should registration', () => {
        cy.get('input[name="login"]').type(login);
        cy.get('input[name="email"]').type(email);
        cy.get('input[name="password"]').type(password);
        cy.get('button').click().wait(500);
        cy.visit('/authorization/login');
        cy.get('input[name="login"]').type(email);
        cy.get('input[name="password"]').type(password);
        cy.get('button').click().url().should('include', '/');
        cy.get(".menu-icon").click();
        cy.get('.main-menu-block').find(`[href="/profile"]`);
    });

    it('should wrong data', () => {
        cy.get('input[name="login"]').type('Vasya15');
        cy.get('input[name="email"]').type('vasya15');
        cy.get('input[name="password"]').type('12345678');
        cy.get('button').click().url().should('include', 'authorization');
        cy.get('.popup .item').should('contain.text', 'Неправильный данные!');
    });
});